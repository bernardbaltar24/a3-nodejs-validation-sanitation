//Declare dependencies and model
const Task = require("../models/tasks");
const express = require("express");
const router = express.Router(); //to handle routing

// --------------TASKS
// 1) CREATE TASK 
router.post("/", (req, res) =>{
	const task = new Task(req.body);
	task.save().then(() => {res.send(task)})
	.catch((e) => {res.status(400).send(e)})
})
//2 GET ALL TASKS

router.get("/", (req, res)=>{
	Task.find().then((tasks) => { return res.status(200).send(tasks)})
	.catch((e) => { return res.status(500).send(e)})	
})
//3)GET ONE TASK
router.get("/:id", (req, res) =>{
	const _id = req.params.id;
	Task.findById(_id).then((task) => {if(!task){
		return res.status(404).send(e)
	} return res.send(task)

	})
	.catch((e) => {return res.status(500).send(e)})
})
//4)UPDATE ONE TASK
router.patch("/:id", (req, res) =>{
	const _id = req.params.id
	Task.findByIdAndUpdate(_id, req.body, {new:true}).then((task) => {
		if(!task){return res.status(404).send(e)}
		return res.send(task)
	})
	.catch((e) => {return res.status(500).send(e)})
})
//5)DELETE ONE TASK
router.delete("/:id", (req, res)=> {
	const _id = req.params.id;
	Task.findByIdAndDelete(_id).then((task) => {if(!task){return res.status(404).send(e)}
		return res.send(task)
	})
	.catch((e) => {return res.status(500).send(e)})
})

module.exports = router;