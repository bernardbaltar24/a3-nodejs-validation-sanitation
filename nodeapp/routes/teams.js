//Declare dependencies and model
const Team = require("../models/teams");
const express = require("express");
const router = express.Router(); //to handle routing


//Create Routes/Endpoints
//1) CREATE
router.post("/", (req, res) =>{
	// return res.send(req.body);
	const team = new Team(req.body);
	//save to DB
	team.save()
	.then(() => {res.send(team)})
	.catch((e) => {res.status(400).send(e)}) //BAD REQUEST (http response status codes)
});

//2)GET ALL
router.get("/", (req, res)=>{
	// return res.send("get all teams");
	Team.find().then((teams) => { return res.status(200).send(teams)})
	.catch((e) => { return res.status(500).send(e)})
})

//3)GET ONE
router.get("/:id", (req, res) =>{
	// return res.send("get a team");
	// console.log(req.params.id)
	const _id = req.params.id;

	//Mongose Models Query
	Team.findById(_id).then((team) => {if(!team){
		//NOT Found
		return res.status(404).send(e)
	} return res.send(team)

	})
	.catch((e) => {return res.status(500).send(e)})
})

//4)UPDATE ONE
router.patch("/:id", (req, res) =>{
	// return res.send("update a team");
	const _id = req.params.id

	Team.findByIdAndUpdate(_id, req.body, {new:true}).then((team) => {
		if(!team){return res.status(404).send(e)}
		return res.send(team)
	})
	.catch((e) => {return res.status(500).send(e)})
})

//5)DELETE ONE
router.delete("/:id", (req, res)=> {
	// return res.send("delete a team");

	const _id = req.params.id;

	Team.findByIdAndDelete(_id).then((team) => {if(!team){return res.status(404).send(e)}
		return res.send(team)
	})
	.catch((e) => {return res.status(500).send(e)})
})
	
module.exports = router;