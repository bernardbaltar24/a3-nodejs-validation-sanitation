//Declare Dependencies

const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const validator = require("validator");
const uniqueValidator = require('mongoose-unique-validator');

//Define your schema
//firstname, lastname, position, timestamps
const memberSchema = new Schema(
	{
		firstName:{
			type: String,
			trim: true,
			default: "J",
			maxlength: 30,
		},

		lastName:{
			type: String,
			trim: true,
			default: "Doe",
			maxlength: 30,
		},

		username: {
			type: String,
			required: true,
			trim: true,
			maxlength: 10,
			unique: true,
			validate(value){
				if(!validator.isAlphanumeric(value)){
					throw new Error("Characters must be letters and numbers only!")
				}
			}
		},

		position: {
			type:String,
			required: true,
			enum: ["instructor", "student", "hr", "admin", "ca"]
		},

		age: {
			type: Number,
			min: 18,
			required: true
		}
	},
	{
		timestamps: true
	}
);

memberSchema.plugin(uniqueValidator);
//Export your model
module.exports = mongoose.model("Member", memberSchema);
